-- Find all artist that has letter "d" in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- Songs that has a length < 230
SELECT * FROM songs WHERE length < 230;

-- Joim "albums" and "songs" tables.
SELECT albums.album_title, songs.song_name, songs.length FROM albums
JOIN songs ON songs.album_id = albums.id;

-- Joins "artists" and "albums" tables
SELECT * FROM artists 
JOIN albums ON albums.artist_id = artists.id
WHERE albums.album_title LIKE "%a%";

-- Sort the album in z-a order
SELECT * FROM albums
ORDER BY album_title DESC
LIMIT 4;

-- Join the "albums" and "songs" tables sort from z-a
SELECT * FROM albums
JOIN songs ON songs.album_id = albums.id
ORDER BY album_title DESC;